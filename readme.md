# Template
  This is a template project for building node project. This template include some basic functionalities. 
### auth
  This comes with a user authentication system. It handles user accounts and cookie-based user sessions.

###### Overview
  The auth module handles authentication. Briefly, authentication verifies a user is who they claim to be.

  The auth system consists of:
  - Login
  - Register
  - It maintain a open connection to users

###### Installation
  Import the auth module
```
    var Auth = require('./auth');
```

  Initialize the auth module, invoke initialize(express instance, http server instance ) methode

```
var express = require('express');
var app = express();

//.... some codes ...

var http = require('http').Server(app);
var auth = Auth.initialize(app, http);

//.... some codes ...
```
###### Extras
  The auth is using [socket.io](http://socket.io/) internally so you can you the socket.io connections, And also you can get the active user list and ther connection id. using the connection is you can push content to specific user,

```
auth.sockets.on('connection', function (socket) {
     auth.emit('an event sent to all connected clients');
});

var activeConnections = auth.getConnectionList();
console.log(activeConnections);
```
