var socket = io.connect();
window.onbeforeunload = function(e) {
  socket.disconnect();
};

socket.emit('test');
socket.on('test_success', function(data) {
  console.log('getting server respons', data);
})
socket.on('time', function(data) {
  var d = new Date(data);
  document.getElementById("timeDisplayer").innerHTML = d.toString();
})

socket.on('no_connection', function(data) {
  var element = document.getElementById("con");
  if (typeof(element) != 'undefined' && element != null){
    element.innerHTML = data;
  }
})
