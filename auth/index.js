var session      = require('express-session');
var passport = require('passport');
var cookieParser = require('cookie-parser');
var flash        = require('req-flash');
var constant = require('../config/constant');

const SECRET_KEY_BASE = constant.SECRET_KEY_BASE;
const MONGO_URL = constant.MONGO_URL;

// used to store sessions and active user  in that session
var currentConnections = [];

module.exports = {
  initialize: function (app, http) {
    require('./routes/passport')(passport);
    // used to store the session
    var MongoStore = require('connect-mongo')(session);
    var sessionStore = new MongoStore({url: MONGO_URL});
    var expressSession = session({
        store: sessionStore,
        key: 'express.sid',
        secret: SECRET_KEY_BASE
    });
    app.use(expressSession);

    app.use(flash());

    // initializing the passport
    app.use(passport.initialize());
    app.use(passport.session()); // persistent login sessions


    // setting the socket.io to listen the http server
    var io = require('socket.io').listen(http);

    // configureing the the passport.socket io for using the user details through socket.io
    var passportSocketIo = require('passport.socketio');
    io.use(passportSocketIo.authorize({
      cookieParser: cookieParser,
      key:          'express.sid',
      secret:       SECRET_KEY_BASE,
      store:        sessionStore,
      success:      onAuthorizeSuccess,
      fail:         onAuthorizeFail,
    }));
    // this function will call when user is authenticated
    function onAuthorizeSuccess(data, accept){
      accept();
    }
    // this function will call when user is not authenticated
    function onAuthorizeFail(data, message, error, accept){
      accept();
    }
    var rout = require('./routes')(app, passport, io, currentConnections);
    return io;
  },
  getConnectionList: function () {
    return currentConnections;
  }
}
