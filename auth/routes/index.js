
module.exports = function (app, passport, io, currentConnections) {

  // used to get the socket.io connections
  io.sockets.on('connection', function (socket) {

    // used to emit the server time to client to each one second
    setInterval(function(){
       io.emit('time', Date.now())
    }, 1000);

    // checcking user is loggedin for the current connection
    if(socket.request){
      if (socket.request.user.local) {

        // if its first time adding to the array
        connection = {
          userId: socket.request.user.local.email,
          socketId: socket.id
        }
        currentConnections.push(connection);

        // on disonnect removing from the list and sending the current count
        socket.on('disconnect', function () {
          var index;
          for(var i=0;i < currentConnections.length; i++ ) {
            if(currentConnections[i].socketId == socket.id){
              index = i;
            }
          }
          currentConnections.splice(index, 1);
          var count = 0;
          for (var i =0; i < currentConnections.length; i++ ) {
            if(currentConnections[i].userId == socket.request.user.local.email){
              count++;
            }
          }

          for (var i =0; i < currentConnections.length; i++ ) {
            if(currentConnections[i].userId == socket.request.user.local.email){
              io.to(currentConnections[i].socketId).emit('no_connection', count)
            }
          }

        });

        // counting the no of logins from the active connections
        var count = 0;
        for (var i =0; i < currentConnections.length; i++ ) {
          if(currentConnections[i].userId == socket.request.user.local.email){
            count++;
          }
        }

        // emit to each user
        for (var i =0; i < currentConnections.length; i++ ) {
          if(currentConnections[i].userId == socket.request.user.local.email){
            io.to(currentConnections[i].socketId).emit('no_connection', count)
          }
        }
      }
    }
  });

  io.sockets.on('connection', function (socket) {

  });

// rendering the index page on '/'
  app.get('/', function (req, res) {
    // checking the yser is login or not if login sending the user details
    if(req.user){
      res.render('index', {user:req.user});
    } else {
      res.render('index');
    }
  });

  // sending the login page on /login
  app.get('/login', function (req, res) {
    res.render('login');
  });

  // on logout removing the user session and redirect to home page
  app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
  });

  //sending the register page on '/register'
  app.get('/register', function (req, res) {
    res.render('register');
  });

  // post data to register
  app.post('/register',passport.authenticate('local-signup', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/reg', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

  //post data to login
  app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

}
